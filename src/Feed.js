import React, { useEffect, useState } from "react";
import "./Feed.css";
import TweetBox from "./TweetBox";
import Post from "./Post";
import db from "./firebase";
import firebase from "firebase";
import FlipMove from "react-flip-move";
import { AvatarGenerator } from "random-avatar-generator";
import Skeleton from "@material-ui/lab/Skeleton";

const Feed = () => {
  const [posts, setPosts] = useState([]);
  const [loading, setLoading] = useState(true);
  const [name, setName] = useState("");
  const [username, setUsername] = useState("");
  const [usernameUnique, setUsernameUnique] = useState("");
  const [avatar, setAvatar] = useState("");

  const getPosts = () => {
    db.collection("posts")
      .orderBy("created", "desc")
      .onSnapshot((snapshot) => {
        setPosts(
          snapshot.docs.map((doc) => {
            return { id: doc.id, ...doc.data() };
          })
        );
        setLoading(false);
      });
  };

  useEffect(() => {
    getPosts();

    let name;
    let username;
    let usernameUnique;
    while (!name) {
      name = prompt("Please enter your name:");
    }
    username = name.replace(/\s/g, "");
    usernameUnique = username.toLowerCase();
    setName(name);
    setUsername(username);
    setUsernameUnique(usernameUnique);
  }, []);

  async function getUsers(usernameUnique) {
    let checkUsernameExists = false;
    return await db
      .collection("users")
      .where("usernameUnique", "==", usernameUnique)
      .get()
      .then((snapshot) => {
        if (!!snapshot.docs.length) {
          checkUsernameExists = true;
          snapshot.docs.map((doc) => {
            const dataUser = doc.data();
            const name = dataUser.name;
            const username = dataUser.username;
            const avatar = dataUser.avatar;
            setName(name);
            setUsername(username);
            setAvatar(avatar);
            return 1;
          });
        }
        return checkUsernameExists;
      });
  }

  useEffect(() => {
    if (usernameUnique) {
      let avatar;
      getUsers(usernameUnique).then((checkUsernameExists) => {
        if (!checkUsernameExists) {
          const generator = new AvatarGenerator();
          avatar = generator.generateRandomAvatar();
          db.collection("users").add({
            usernameUnique: usernameUnique,
            username: username,
            name: name,
            avatar: avatar,
            created: firebase.firestore.FieldValue.serverTimestamp(),
          });
          setAvatar(avatar);
        }
      });
    }
  }, [name, username, usernameUnique]);

  return (
    <div className="feed">
      <div className="feed__header">
        <h2>Home</h2>
      </div>
      <TweetBox name={name} username={username} avatar={avatar} />
      {!loading ? (
        <FlipMove>
          {posts.map((post, index) => (
            <Post
              key={post.id}
              id={post.id}
              displayName={post.displayName}
              username={post.username}
              verified={post.verified}
              text={post.text}
              image={post.image}
              avatar={post.avatar}
            />
          ))}
        </FlipMove>
      ) : (
        <>
          <Skeleton variant="circle" width={40} height={40} />
          <Skeleton variant="text" />
          <Skeleton variant="text" />
          <Skeleton variant="rect" height={118} />
        </>
      )}
    </div>
  );
};

export default Feed;

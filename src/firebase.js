import firebase from "firebase";

const firebaseConfig = {
  apiKey: "AIzaSyBApo48oI1H0N58N7JYqMab0RMUZgEQ6qY",
  authDomain: "twitter-clone-d7936.firebaseapp.com",
  databaseURL: "https://twitter-clone-d7936.firebaseio.com",
  projectId: "twitter-clone-d7936",
  storageBucket: "twitter-clone-d7936.appspot.com",
  messagingSenderId: "413518974457",
  appId: "1:413518974457:web:43565236329b5217948128",
  measurementId: "G-0KDW88J6XP",
};

const firebaseApp = firebase.initializeApp(firebaseConfig);

const db = firebaseApp.firestore();

export default db;
